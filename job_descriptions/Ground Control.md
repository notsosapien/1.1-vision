# Ground Control
#job descriptions/ground control#

## Overview
The project manager is responsible for tracking the progress of a job through the system. They should have the ability to effectively asses the progress and requirements of a project, acting as a facilitator between the team and the client and ensuring that expectations are managed both internally and externally.



## Responsibilities
- Providing clear and detailed briefs and specifications
- Developing objective evaluation criteria for each project
- Managing client expectations
- Managing team objectives
- Motivating and evaluating team performance
- Watching out for scope creep
- Excellent and early communication of challenges and potential pitfalls
- They should be able to offer the team strategic input and assist each respective party with evaluating the quality of their out put.
- Being able to correlate feature requests/issue tracking with git commits and toggle entries to provide team members with accurate feedback based on their performance and project health.
- Testing and quality control.
-  Ensuring that work is handled efficiently and managing project dependencies


## Communication Strategy
One of the essential roles of this position is effectively managing a projects communication strategy. key considerations are:

- Scheduling client communication (when and how often to provide feedback to the client)
- Scheduling reviews and keeping track of the teams progress with out getting in their hair
- Providing clear guidelines for internal communication and be able to tell the team when they need to call a meeting, pick up the phone, send a slack message, workshop a challenge, discuss it over a beer, put it on a post it, write it on their belly and go for a swim

## You'll love your job if...
Data is your friend. You determine how best to manage it. Project Management tools, spreadsheets, trello boards, slack feeds, post it notes, mind maps and screenshots make you happy.

## You’ll know you’re good at your job if:





